%{
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h>
#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>

#define DOCLINES 100
#define DOCCOLUMNS 100
%}

REE    [vV][iIl][kK][tT7][oO0][rR]

%%

{REE}     {return 0xFF;}

.       {return yytext[0];}
%%

int docLength = 1;
// Cursor stuff
static int line = 0;
static int column = 0;

char buffer[DOCLINES][DOCCOLUMNS];

pthread_t stupid;
sem_t doRender, doKurt;

void *render(void *arg) {
	// Forever and ever
	for(;;) {
		// Wait for the signal
		sem_wait(&doRender);
		for(int i = 0; i < docLength; i++) {
			move(i, 0);
			clrtoeol();
			printw("%s", buffer[i]);
		}
		// Move cursor back
		move(line, column);

		refresh();
	}
}

void *replace_kurt(void *arg) {
	for(;;) {
		// Wait for user input
		sem_wait(&doKurt);
		for(int i = 0; i < docLength; i++) {
			uint8_t token = 0;
			int lexIndex = 0;
			yy_scan_string(buffer[i]);
			while(buffer[i][lexIndex] != 0) {
				token = yylex();
				//printw("hej %d  %c, %d", token, token, lexIndex);refresh();
				if( token == 0xFF) {
					buffer[i][lexIndex+2] = 'c';
					//memcpy(&buffer[i][lexIndex+2], "c");
					lexIndex += 5;
				}
				lexIndex++;
			}
		}

		// We are done, rendering can be done
		sem_post(&doRender);
	}
}

// Move the cursor back until it reaches a valid char. Otherwise we can type in the void.
void correct_column() {
	// Just move forward
	for(int i = 0; i < DOCCOLUMNS; i++) {
		if( i == column ) {
			// It's fine
			return;
		}
		if( buffer[line][i] == 0 ) {
			// Oh no end of line, up date column pos
			column = i;
			return;
		}
	}
}

void *handle_input(void *arg) {
	while(1) {
		__label__ cursor_update;
		int nextChar = getch();
		switch(nextChar) {
			case 10:
				line++;
				if( line >= docLength ) {
					docLength++;
				}
				column=0;
				break;
			case 0x08:
			case 0x7F:
			case KEY_BACKSPACE:
				// Backspace
				column = column ? column - 1 : 0;
				buffer[line][column] = 0;
				break;
			case KEY_LEFT:
				column = column ? column - 1 : 0;
				goto cursor_update;
				break;
			case KEY_RIGHT:
				column = buffer[line][column] != 0 ? column + 1 : column;
				goto cursor_update;
				break;
			case KEY_UP:
				line = line ? line - 1 : 0;
				correct_column();
				goto cursor_update;
				break;
			case KEY_DOWN:
				line++;
				if( line >= docLength) {
					// ABORT GO BACK
					line--;
					break;
				}
				correct_column();
				goto cursor_update;
				break;
			default:
				buffer[line][column] = nextChar;
				column++;
				break;
		}
		// Something changed give the signal
		sem_post(&doKurt);
		continue;
		cursor_update:
		move(line, column);
	}
}

void *take_backup(void *arg) {
	// Infinite loop
	for(;;) {
		// Open the file as write
		FILE *f = fopen("hej.txt", "w");
		// For each line
		for(int i = 0; i < docLength; i++) {
			// Write line
			fprintf(f, "%s\n", buffer[i]);
		}
		fclose(f);
		
		// Wait another second
		sleep(1);
	}
}


int main(int argc, char *argv[])  {

	// Make sure we render on start
	sem_init(&doRender, 0, 1);
	sem_init(&doKurt, 0, 0);

	initscr();
	cbreak();
	noecho();
	// Arrow keys
	keypad(stdscr, TRUE);

	int err;

	err = pthread_create(&stupid, NULL, take_backup, NULL);
	if( err ) {
		printf("Could not create thread take_backup\n");
		return -1;
	}
	err = pthread_create(&stupid, NULL, render, NULL);
	if( err ) {
		printf("Could not create thread render\n");
		return -1;
	}
	err = pthread_create(&stupid, NULL, replace_kurt, NULL);
	if( err ) {
		printf("Could not create thread replace_kurt\n");
		return -1;
	}

	pthread_t threadId;

	err = pthread_create(&threadId, NULL, handle_input, NULL);
	if( err ) {
		printf("Could not create thread handle_input\n");
		return -1;
	}
	pthread_join(threadId, NULL);
	endwin();

	return 0;
}
