let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;

in stdenv.mkDerivation {
  name = "graph-theory-env";

  buildInputs = with pkgs; [
    flex
    bison
    ncurses
  ];
}
